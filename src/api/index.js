const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

const earthRoute = require('./routes/earth');

app.use('/earth', earthRoute);

// Custom error for not existing routes
app.get('*', (req, res) => {
    return res.status(404).send({
        'error': 'Route not found.'
    });
});

module.exports = app;