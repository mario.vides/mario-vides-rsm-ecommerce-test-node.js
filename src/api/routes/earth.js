const router = require('express').Router();
const earthService = require('../../services/earth');

router.get('/', earthService.defaultData);
router.post('/', earthService.returnApiData);

module.exports = router;