require('dotenv').config();

config = {
    port: process.env.PORT || 3000,
    apiURL: process.env.NASA_API_URL,
    apiKey: process.env.NASA_API_KEY
};

module.exports = config;