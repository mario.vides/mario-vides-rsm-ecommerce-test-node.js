// Project configuration file
const config = require('./config');

// Application routes and middlewares
const app = require('./api');

app.set('views', './src/views');
app.set('view engine', 'pug');

// Start server
app.listen(config.port, () => console.log('Server running at port ' + config.port));