const axios = require('axios');
const config = require('../config');

const nasaUrl = config.apiURL;
const apiKey = config.apiKey;
const response = { 
    title: 'NASA aerial photos', 
    pageHeader: 'NASA pictures',
    map: {
        src: ''
    }
};

exports.defaultData = async (req, res) => {
    res.render('map', response);
};

exports.returnApiData = async (req, res) => {
    const urlParams = {};

        const coordinates = req.body.coordinates;
        const coordinateRegex = /(-?[0-9]+[.][0-9]+), (-?[0-9]+[.][0-9]+)/;

        if (!coordinates.match(coordinateRegex)) {
            response.error = { 
                msg: 'Please enter a valid coordinate value separated by commas, e.g.: "40.46367, -3.74922"'
            };
            return res.render('map', response);
        }

        const coordinateArray = coordinates.split(', ');

        urlParams.latitude = coordinateArray[0];
        urlParams.longitude = coordinateArray[1];

        const requestUrl = `${ nasaUrl }?lon=${ urlParams.longitude }&lat=${ urlParams.latitude }&dim=0.25&api_key=${apiKey}`;

    try {
        const apiData = await axios.get(requestUrl);

        if (apiData.data) {
            response.map.src = apiData.data.url;
            response.coordinates = urlParams.latitude + ', ' + urlParams.longitude;
            response.error = {};
        }

        res.render('map', response);

    } catch (error) {
        if (error.response) {
            response.error = error.response.data;
            res.render('map', response);
        } else {
            res.status(500).send(error);
        }
    }
};
