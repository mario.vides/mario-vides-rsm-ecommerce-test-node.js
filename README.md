# Mario Vides - RSM eCommerce test - Node.js
----
This is an Node project to display images from NASA API.

## Packages
    - express: Node.js framework
    - dotenv: Loads environment variables from .env to process.env
    - pug: As template engine
    - bootstrap: This was used via CDN just to provide styles to the webapp.

## Installation
After cloning the package, use npm as package manager and install the dependencies:
```bash
npm install
```
Once it finishes, we need to configure our environment variables, copy the .env.example into a new file called ".env" and place the following information:
 - PORT= /* Port where we want to run the server */
 - NASA_API_URL= /* I left the URL to fetch the pictures, keep the same */
 - NASA_API_KEY= /* API Key generated at https://api.nasa.gov/index.html#apply-for-an-api-key */

## Project structure
 - .env.example: Template for creating our .env file
 - src: Where all the magic happens!
    * app.js: Entry point of the application.
    * api: Contains the files where routes are defined with their middlewares (In this case we don't use middlewares).
    * config: Has a file that reads the environment variables used for configuration.
    * services: Contains the business logic.
    * views: Contains the pug files for templates.
 - public: Here we have the CSS of the application, but can be used with other external files like JS scripts.

## USAGE
To run locally:
```bash
npm start
```
The application is used under the route "/earth"
E.g. If you're running the application under "http://localhost:3000/" the route is "http://localhost:3000/earth/"

## Author
Mario José Vides Padilla - mariojvip@hotmail.com
